using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class ArmCannon : MonoBehaviour
{
    [SerializeField] private InputActionProperty shootCannonProperty;
    public float projectileSpeed = 20f;

    private XRRayInteractor rayInteractor;
    private RaycastHit hit;

    public GameObject EnergyPrefab;

    // Start is called before the first frame update
    void Start()
    {
        rayInteractor = GetComponent<XRRayInteractor>();

        // Get Shoot Action
        shootCannonProperty.action.performed += OnButtonPress;
        shootCannonProperty.action.canceled += OnButtonRelease;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnButtonPress(InputAction.CallbackContext obj)
    {
        rayInteractor.TryGetCurrent3DRaycastHit(out hit);
        GameObject energyShot = Instantiate(EnergyPrefab, transform.position, Quaternion.identity);

        EnergyBlast blast = energyShot.GetComponent<EnergyBlast>();
        Rigidbody body = energyShot.GetComponent<Rigidbody>();

        blast.Setup(transform.position);
        // Move projectile towards its location
        body.AddForce(transform.TransformDirection(Vector3.forward) * projectileSpeed, ForceMode.VelocityChange);
    }

    private void OnButtonRelease(InputAction.CallbackContext obj)
    {
     
    }
}
