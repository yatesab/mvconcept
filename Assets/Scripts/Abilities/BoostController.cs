using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BoostController : MonoBehaviour
{
    private ThreadShooter _threadShooter;
    private float _coolDown = 0f;
    private bool _isBoosting;
    private Coroutine _boostCoroutine;

    // Audio Props
    private AudioSource _boostAudioSource;
    private string _boostAudioName = "Air_Boost";

    [Header("Air Boost Values")]
    [SerializeField] private float boostAmount = 30f;
    [SerializeField] private float coolDownTime = 5f;

    [Header("Line Boost Values")]
    [SerializeField] private float lineSpring = 20f;
    [SerializeField] private float lineDamper = 10f;

    [Space]
    [SerializeField] private InputActionProperty boostProperty;

    // Start is called before the first frame update
    void Start()
    {
        _threadShooter = GetComponent<ThreadShooter>();

        // Pull Audio Sources for playback
        AudioSource[] audioSources = GetComponents<AudioSource>();
        foreach (AudioSource audio in audioSources)
        {
            if (audio.clip.name == _boostAudioName)
            {
                _boostAudioSource = audio;
            }
        }

        // Get Boost Action
        boostProperty.action.performed += OnBoostPress;
        boostProperty.action.canceled += OnBoostRelease;
    }

    void Update()
    {
        if (_coolDown > 0f)
        {
            _coolDown -= Time.deltaTime;
        }
    }

    private void AirBoost()
    {
        //Play Thread Shooting Sound
        if (_boostAudioSource != null)
        {
            _boostAudioSource.Play();
        }

        _threadShooter.GetPlayerBody().AddForce(transform.forward * boostAmount, ForceMode.VelocityChange);
    }

    private void OnBoostPress(InputAction.CallbackContext obj)
    {
        if (_coolDown > 0) return;

        _coolDown = coolDownTime;

        //Play Thread Shooting Sound
        if (_boostAudioSource != null)
        {
            _boostAudioSource.Play();
        }

        if (_threadShooter.isGrappling)
        {
            _boostCoroutine = StartCoroutine(LineBoost());
        } else
        {
            AirBoost();
        }
    }

    private void OnBoostRelease(InputAction.CallbackContext obj)
    {
        if (_boostCoroutine != null)
        {
            StopCoroutine(_boostCoroutine);
            _threadShooter.GetPlayerBody().useGravity = true;
            _isBoosting = false;
        }
    }

    private IEnumerator LineBoost()
    {
        _isBoosting = true;

        Rigidbody playerBody = _threadShooter.GetPlayerBody();
        SpringJoint joint = _threadShooter.GetJoint();

        //playerBody.AddForce(-playerBody.velocity, ForceMode.VelocityChange);
        playerBody.useGravity = false;
        joint.spring = lineSpring;
        joint.damper = lineDamper;

        // Start Lowering Distance
        while (joint != null && _threadShooter.DistanceToGrapplePoint() > 0f)
        {
            if (joint.maxDistance > 0f)
            {
                joint.maxDistance -= 10f;
            }
            else
            {
                Vector3 newForce = (_threadShooter.GetEndPoint() - _threadShooter.GetAttachPoint().position) * Time.deltaTime;
                playerBody.AddForce(newForce, ForceMode.VelocityChange);
            }

            yield return new WaitForEndOfFrame();
        }

        playerBody.useGravity = true;

        _isBoosting = false;
    }
}
