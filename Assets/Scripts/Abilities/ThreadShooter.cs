using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class ThreadShooter : MonoBehaviour
{
    public enum hands
    {
        Left,
        Right
    };
    private Transform _playerPosition, _attachPoint, _grapplePosition;
    private Rigidbody _playerBody;
    private XRRayInteractor _rayInteractor;
    private LineRenderer _threadRenderer;
    private Vector3 _endPoint, _grappleLocalOffset;
    private RaycastHit _controllerRaycastHit;
    private SpringJoint _joint;

    //Coroutines
    private Coroutine _grappleCoroutine;

    // Audio Props
    private AudioSource _threadAudioSource, _retractAudioSource;
    private string _threadAudioName = "Thread_Shot", _retractAudioName = "Retract_Thread";

    [Header("Hand Side")]
    [SerializeField] private hands _handSide;

    [Header("Thread Values")]
    [SerializeField] private Material lineMaterial;
    [SerializeField] private float lineWidth = 0.01f;

    [Header("Joint Values")]
    [SerializeField] private float grappleSpeed = 100f;
    [SerializeField] private float startingTension = 0.9f;
    [SerializeField] public float startingSpring = 10f;
    [SerializeField] public float startingDamper = 5f;

    [Header("Retract Values")]
    [SerializeField] private float retractSpeed = 0.3f;

    [Space]
    [SerializeField] private InputActionProperty grappleProperty;
    [SerializeField] private InputActionProperty retractProperty;
    private InputAction _retractAction;

    // Flags
    public bool isGrappling, isRetracting;

    // Start is called before the first frame update
    void Start()
    {
        // Get Objects
        _playerPosition = GameObject.Find("/PlayerRig").transform;
        _attachPoint = GameObject.Find("/PlayerModel/PlayerWaist/Silkworm" + _handSide).transform;
        _playerBody = GetComponentInParent<Rigidbody>();
        _rayInteractor = GetComponent<XRRayInteractor>();

        // Get Thread Action
        grappleProperty.action.performed += OnGrapplePress;
        grappleProperty.action.canceled += OnGrappleRelease;

        // Get Retract Action
        _retractAction = retractProperty.action;
        _retractAction.performed += OnRetractPress;
        _retractAction.canceled += OnRetractRelease;
        _retractAction.Enable();

        // // Get Audio Sources for playback
        // AudioSource[] audioSources = GetComponents<AudioSource>();
        // foreach (AudioSource audio in audioSources)
        // {
        //     if (audio.clip.name == _threadAudioName)
        //     {
        //         _threadAudioSource = audio;
        //     }
        //     if (audio.clip.name == _retractAudioName)
        //     {
        //         _retractAudioSource = audio;
        //     }
        // }
    }

    void LateUpdate()
    {
        // Check for connection and update connectedAnchor incase it moved
        if (_joint != null)
        {
            _endPoint = _grapplePosition.TransformPoint(_grappleLocalOffset);
            _joint.connectedAnchor = _endPoint;
        }

        //Draw the thread for this frame
        if (_threadRenderer != null)
        {
            DrawThread();
        }
    }

    /********************
    * Utility Functions *
    *********************/
    private void DrawThread()
    {
        // Set the start point at the controller position
        _threadRenderer.SetPosition(0, _attachPoint.position);

        _threadRenderer.SetPosition(1, _endPoint);
    }


    private void SetupJoint()
    {
        _grapplePosition = _controllerRaycastHit.transform;
        _endPoint = _controllerRaycastHit.point;

        //Create the joint
        _joint = _playerPosition.gameObject.AddComponent<SpringJoint>();

        // Set up player anchor and object anchor
        _joint.autoConfigureConnectedAnchor = false;
        _joint.enableCollision = true;
        _joint.anchor = _playerPosition.InverseTransformPoint(_attachPoint.position);

        _grappleLocalOffset = _grapplePosition.InverseTransformPoint(_endPoint);

        _joint.connectedAnchor = _grapplePosition.TransformPoint(_grappleLocalOffset);

        // Create initial maxDistance for joint and other spring settings
        float distanceFromPoint = Vector3.Distance(_attachPoint.position, _endPoint);
        _joint.maxDistance = distanceFromPoint * startingTension;
        _joint.massScale = _playerBody.mass;
        _joint.spring = startingSpring;
        _joint.damper = startingDamper;
    }

    private void SetupLineRenderer()
    {
        // Add Line Renderer
        _threadRenderer = _attachPoint.gameObject.AddComponent<LineRenderer>();

        // Configure Line Renderer
        _threadRenderer.startWidth = lineWidth;
        _threadRenderer.endWidth = lineWidth;
        _threadRenderer.material = lineMaterial;
    }

    private void DestroyGrapple()
    {
        if (_joint != null) Destroy(_joint);
        if (_threadRenderer != null) Destroy(_threadRenderer);
    }

    public void LowerJointMaxDistance(float distanceSubtract)
    {
        if (_joint == null || _joint.maxDistance < 0f) return;
        _joint.maxDistance -= distanceSubtract;

        // if (_retractAudioSource.isPlaying) return;
        // _retractAudioSource.Play();
    }

    /******************
     * Button Actions *
     ******************/
    private void OnGrapplePress(InputAction.CallbackContext obj)
    {
        if (_joint) return;

        if (_rayInteractor.TryGetCurrent3DRaycastHit(out _controllerRaycastHit))
        {
            isGrappling = true;

            SetupLineRenderer();

            //Play Thread Shooting Sound
            if (_threadAudioSource != null)
            {
                _threadAudioSource.Play();
            }

            _grappleCoroutine = StartCoroutine(FireGrapple());
        }
    }

    private void OnRetractPress(InputAction.CallbackContext obj)
    {
        isRetracting = true;

        StartCoroutine(RetractPlayer());
    }

    private void OnGrappleRelease(InputAction.CallbackContext obj)
    {
        if (_grappleCoroutine != null)
        {
            StopCoroutine(_grappleCoroutine);
        }

        DestroyGrapple();
        isRetracting = false;
        isGrappling = false;

        // if (_retractAudioSource.isPlaying)
        // {
        //     _retractAudioSource.Stop();
        // }
    }

    private void OnRetractRelease(InputAction.CallbackContext obj)
    {
        if (_joint != null)
        {
            isRetracting = false;
            // if (_retractAudioSource.isPlaying)
            // {
            //     _retractAudioSource.Stop();
            // }
        }
    }

    /**************
     * Coroutines *
     **************/
    private IEnumerator FireGrapple()
    {
        // Get starting point
        _endPoint = _attachPoint.position;

        // Start Moving _endpoint towards the raycastHit
        while (Vector3.Distance(_endPoint, _controllerRaycastHit.point) > 0f )
        {
            _endPoint = Vector3.MoveTowards(_endPoint, _controllerRaycastHit.point, grappleSpeed);
            yield return new WaitForEndOfFrame();
        }

        SetupJoint();
    }

    private IEnumerator RetractGrapple()
    {
        if (_joint != null) Destroy(_joint);

        // Start Moving _endpoint towards the raycastHit
        while (Vector3.Distance(_endPoint, _attachPoint.position) > 1f)
        {
            _endPoint = Vector3.MoveTowards(_endPoint, _attachPoint.position, grappleSpeed * _playerBody.velocity.magnitude);
            yield return new WaitForEndOfFrame();
        }

        if (_threadRenderer != null) Destroy(_threadRenderer);
    }

    private IEnumerator RetractPlayer()
    {
        while (_retractAction.ReadValue<float>() == 1)
        {
            LowerJointMaxDistance(retractSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    /*****************
     * Get Functions *
     *****************/
    public Rigidbody GetPlayerBody()
    {
        return _playerBody;
    }

    public SpringJoint GetJoint()
    {
        return _joint;
    }

    public float DistanceToGrapplePoint()
    {
        return Vector3.Distance(_attachPoint.position, _endPoint);
    }

    public Transform GetAttachPoint()
    {
        return _attachPoint;
    }

    public Vector3 GetEndPoint()
    {
        return _endPoint;
    }
}
