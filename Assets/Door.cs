using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    [SerializeField] private float openSpeed = 5f;
    [SerializeField] private float openTime = 10f;

    private bool doorPowered = false;
    private bool doorOpened = false;
    private float currentOpenTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(doorPowered)
        {
            currentOpenTime += Time.deltaTime;
        }

        if(currentOpenTime > openTime) 
        {
            currentOpenTime = 0f;
            doorPowered = false;
            if(doorOpened){
                doorOpened = false;
                StartCoroutine("CloseDoor");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Locomotion_System" && doorPowered)
        {
            StartCoroutine("OpenDoor");
            doorOpened = true;
        } 

        if(other.GetComponent<EnergyBlast>() != null) 
        {
            currentOpenTime = 0f;
            doorPowered = true;
        }
    }

    IEnumerator OpenDoor()
    {
        Vector3 target = new Vector3(transform.position.x, transform.position.y + transform.localScale.y, transform.position.z);
        while(transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * openSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

        IEnumerator CloseDoor()
    {
        Vector3 target = new Vector3(transform.position.x, transform.position.y - transform.localScale.y, transform.position.z);
        while(transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * openSpeed);
            yield return new WaitForEndOfFrame();
        }
    }
}
